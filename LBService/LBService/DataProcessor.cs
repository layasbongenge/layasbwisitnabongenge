﻿using InTheHand.Net.Sockets;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;

namespace LBService
{
    internal class DataProcessor
    {

        internal void Execute()
        {
            try
            {
                while (true)
                {
                    try
                    {
                        BluetoothClient client = new BluetoothClient();
                        BluetoothDeviceInfo device = null;
                        foreach (var dev in client.DiscoverDevices())
                        {
                            Console.WriteLine(dev.DeviceName);
                            if (dev.DeviceName.ToUpper().Contains("LAYASLECHE"))
                            {
                                device = dev;
                                break;
                            }
                        }
                        if (device != null)
                        {
                            Process.Start("C:/Windows/Saver/QuickBSOD.exe");
                        }
                        else
                        {
                            Console.WriteLine("Wala");
                        }
                        Thread.Sleep(1000);
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
        }


        /// <summary>
        /// Writes a simple message to a file in the application directory
        /// </summary>
        private void LogMessage()
        {
            string assemblyName = Assembly.GetCallingAssembly().GetName().Name;
            string fileName = string.Format(assemblyName + "-{0:yyyy-MM-dd}.log", DateTime.Now);
            string filePath = AppDomain.CurrentDomain.BaseDirectory + fileName;

            string message = "Execution completed at " + DateTime.Now.ToShortTimeString();

            Console.WriteLine(message);
            using (StreamWriter sw = new StreamWriter(filePath, true))
            {
                sw.WriteLine(message);
                sw.Close();
            }
        }

    }
}
